(* -------------------------------------------------------------------------- *)
// hello.fs
//
//
// Author : Leonardone @ NEETSDKASU
// License: MIT
(* -------------------------------------------------------------------------- *)

open System.Runtime.Serialization
open System.IO
open System.Numerics

(* -------------------------------------------------------------------------- *)
// Solver

type Div  = (int64 * int64)
type Divp = (Div * Div)
type Divs = Divp array 

type DivOF  = (BigInteger * BigInteger)
type DivpOF = (DivOF * DivOF)
type DivsOF = DivpOF array 

module Solver =
    let idleAnswer =
        let v0 = ((0L, 1L), (0L, 1L))
        let v1 = ((0L, 1L), (1L, 1L))
        let v2 = ((1L, 1L), (1L, 1L))
        let v3 = ((1L, 1L), (0L, 1L))
        ([| v0; v1; v2; v3 |], [| [|0; 1; 2; 3|] |], [| v0; v1; v2; v3 |])
    
    let solve (polys : Divs array) (lines : (Divp * Divp) array) : (Divs * (int array array) * Divs) =
        let v0 = ((0L, 1L), (0L, 1L))
        let v1 = ((0L, 1L), (1L, 1L))
        let v2 = ((1L, 1L), (1L, 1L))
        let v3 = ((1L, 1L), (0L, 1L))
        let g ((xc1, xp1), (yc1, yp1), mx1, my1) ((xc2, xp2), (yc2, yp2)) =
            let mx2 = (float xc2) / (float xp2)
            let my2 = (float yc2) / (float yp2)
            let (xc3, xp3, mx3) = if mx1 > mx2 then (xc1, xp1, mx1) else (xc2, xp2, mx2)
            let (yc3, yp3, my3) = if my1 > my2 then (yc1, yp1, my1) else (yc2, yp2, my2)
            ((xc3, xp3), (yc3, yp3), mx3, my3)
        let f r divs = Array.fold g r divs
        let start = Array.fold f ((0L, 1L), (0L, 1L), 0.0, 0.0) polys
        let gg ((xc1, xp1), (yc1, yp1), mx1, my1) ((xc2, xp2), (yc2, yp2)) =
            let mx2 = (float xc2) / (float xp2)
            let my2 = (float yc2) / (float yp2)
            let (xc3, xp3, mx3) = if mx1 < mx2 then (xc1, xp1, mx1) else (xc2, xp2, mx2)
            let (yc3, yp3, my3) = if my1 < my2 then (yc1, yp1, my1) else (yc2, yp2, my2)
            ((xc3, xp3), (yc3, yp3), mx3, my3)
        let ff r divs = Array.fold gg r divs
        let ((xc, xp), (yc, yp), mx, my) = Array.fold ff start polys
        // powerup
        let ((uxc, uxp), (uyc, uyp), _, _) = Array.fold f ((xc, xp), (yc, yp), mx, my) polys
        let (dvx, x1c, x2c, xxp, dvy, y1c, y2c, yyp) =
            let (x1c, x2c, xxp) = if xp = uxp then (xc, uxc, xp) else (xc * uxp, uxc * xp, xp * uxp)
            let (y1c, y2c, yyp) = if yp = uyp then (yc, uyc, yp) else (yc * uyp, uyc * yp, yp * uyp)
            let dvx = x1c < x2c && x2c < (x1c + xxp)
            let dvy = y1c < y2c && y2c < (y1c + yyp)
            (dvx, x1c, x2c, xxp, dvy, y1c, y2c, yyp)
        let z0 = ((xc, xp), (yc, yp))
        let z1 = ((xc + xp, xp), (yc, yp))
        let z2 = ((xc + xp, xp), (yc + yp, yp))
        let z3 = ((xc, xp), (yc + yp, yp))
        let (lftx, rghx) = ((0L, 1L), (1L, 1L))
        let (btmy, topy) = ((0L, 1L), (1L, 1L))
        match (dvx, dvy) with
        | (false, true) ->
            let (v0, v1, v2) =
                ((lftx, btmy), (lftx, (y2c - y1c, yyp)), (lftx, topy))
            let (v3, v4, v5) =
                ((rghx, btmy), (rghx, (y2c - y1c, yyp)), (rghx, topy))
            let mvy = y2c - (yyp + y1c - y2c)
            let (lftx, rghx) = ((xc, xp), (xc + xp, xp))
            let (z0, z1, z2) =
                ((lftx, (y1c, yyp)), (lftx, (y2c, yyp)), (lftx, (mvy, yyp)))
            let (z3, z4, z5) =
                ((rghx, (y1c, yyp)), (rghx, (y2c, yyp)), (rghx, (mvy, yyp)))
            let rr =
                [| [| 0; 1; 4; 3 |]; [| 1; 2; 5; 4 |] |]
            ([| v0; v1; v2; v3; v4; v5 |], rr, [| z0; z1; z2; z3; z4; z5 |])
        | (true, _) ->
            let (v0, v1, v2) =
                ((lftx, btmy), ((x2c - x1c, xxp), btmy), (rghx, btmy))
            let (v3, v4, v5) =
                ((lftx, topy), ((x2c - x1c, xxp), topy), (rghx, topy))
            let mvx = x2c - (xxp + x1c - x2c)
            let (btmy, topy) = ((yc, yp), (yc + yp, yp))
            let (z0, z1, z2) =
                (((x1c, xxp), btmy), ((x2c, xxp), btmy), ((mvx, xxp), btmy))
            let (z3, z4, z5) =
                (((x1c, xxp), topy), ((x2c, xxp), topy), ((mvx, xxp), topy))
            let rr =
                [| [| 0; 1; 4; 3 |]; [| 1; 2; 5; 4 |] |]
            ([| v0; v1; v2; v3; v4; v5 |], rr, [| z0; z1; z2; z3; z4; z5 |])
        | _ ->
            ([| v0; v1; v2; v3 |], [| [|0; 1; 2; 3|] |], [| z0; z1; z2; z3 |])
        
    
    let One = BigInteger.One
    let Zero = BigInteger.Zero
    
    let solveOF (polys : DivsOF array) (lines : (DivpOF * DivpOF) array) : (DivsOF * (int array array) * DivsOF) =
        let v0 = ((Zero, One), (Zero, One))
        let v1 = ((Zero, One), (One, One))
        let v2 = ((One, One), (One, One))
        let v3 = ((One, One), (Zero, One))
        let g ((xc1, xp1), (yc1, yp1)) ((xc2, xp2), (yc2, yp2)) =
            let dx1 = xc1 * xp2
            let dx2 = xc2 * xc1
            let dy1 = yc1 * yp2
            let dy2 = yc2 * yp1
            let x3 = if dx1 > dx2 then (xc1, xp1) else (xc2, xp2)
            let y3 = if dy1 > dy2 then (yc1, yp1) else (yc2, yp2)
            (x3, y3)
        let f r divs = Array.fold g r divs
        let start = Array.fold f ((Zero, One), (Zero, One)) polys
        let gg ((xc1, xp1), (yc1, yp1)) ((xc2, xp2), (yc2, yp2)) =
            let dx1 = xc1 * xp2
            let dx2 = xc2 * xc1
            let dy1 = yc1 * yp2
            let dy2 = yc2 * yp1
            let x3 = if dx1 < dx2 then (xc1, xp1) else (xc2, xp2)
            let y3 = if dy1 < dy2 then (yc1, yp1) else (yc2, yp2)
            (x3, y3)
        let ff r divs = Array.fold gg r divs
        let ((xc, xp), (yc, yp)) = Array.fold ff start polys
        // powerup
        let ((uxc, uxp), (uyc, uyp)) = Array.fold f ((xc, xp), (yc, yp)) polys
        let (dvx, x1c, x2c, xxp, dvy, y1c, y2c, yyp) =
            let (x1c, x2c, xxp) = if xp = uxp then (xc, uxc, xp) else (xc * uxp, uxc * xp, xp * uxp)
            let (y1c, y2c, yyp) = if yp = uyp then (yc, uyc, yp) else (yc * uyp, uyc * yp, yp * uyp)
            let dvx = x1c < x2c && x2c < (x1c + xxp)
            let dvy = y1c < y2c && y2c < (y1c + yyp)
            (dvx, x1c, x2c, xxp, dvy, y1c, y2c, yyp)
        let z0 = ((xc, xp), (yc, yp))
        let z1 = ((xc + xp, xp), (yc, yp))
        let z2 = ((xc + xp, xp), (yc + yp, yp))
        let z3 = ((xc, xp), (yc + yp, yp))
        let (lftx, rghx) = ((Zero, One), (One, One))
        let (btmy, topy) = ((Zero, One), (One, One))
        match (dvx, dvy) with
        | (false, true) ->
            let (v0, v1, v2) =
                ((lftx, btmy), (lftx, (y2c - y1c, yyp)), (lftx, topy))
            let (v3, v4, v5) =
                ((rghx, btmy), (rghx, (y2c - y1c, yyp)), (rghx, topy))
            let mvy = y2c - (yyp + y1c - y2c)
            let (lftx, rghx) = ((xc, xp), (xc + xp, xp))
            let (z0, z1, z2) =
                ((lftx, (y1c, yyp)), (lftx, (y2c, yyp)), (lftx, (mvy, yyp)))
            let (z3, z4, z5) =
                ((rghx, (y1c, yyp)), (rghx, (y2c, yyp)), (rghx, (mvy, yyp)))
            let rr =
                [| [| 0; 1; 4; 3 |]; [| 1; 2; 5; 4 |] |]
            ([| v0; v1; v2; v3; v4; v5 |], rr, [| z0; z1; z2; z3; z4; z5 |])
        | (true, _) ->
            let (v0, v1, v2) =
                ((lftx, btmy), ((x2c - x1c, xxp), btmy), (rghx, btmy))
            let (v3, v4, v5) =
                ((lftx, topy), ((x2c - x1c, xxp), topy), (rghx, topy))
            let mvx = x2c - (xxp + x1c - x2c)
            let (btmy, topy) = ((yc, yp), (yc + yp, yp))
            let (z0, z1, z2) =
                (((x1c, xxp), btmy), ((x2c, xxp), btmy), ((mvx, xxp), btmy))
            let (z3, z4, z5) =
                (((x1c, xxp), topy), ((x2c, xxp), topy), ((mvx, xxp), topy))
            let rr =
                [| [| 0; 1; 4; 3 |]; [| 1; 2; 5; 4 |] |]
            ([| v0; v1; v2; v3; v4; v5 |], rr, [| z0; z1; z2; z3; z4; z5 |])
        | _ ->
            ([| v0; v1; v2; v3 |], [| [|0; 1; 2; 3|] |], [| z0; z1; z2; z3 |])

(* -------------------------------------------------------------------------- *)

let DATA_DIR         = Path.Combine (Directory.GetCurrentDirectory(), "data")
let SNAPSHOT_FILE    = Path.Combine (DATA_DIR, "snapshot.txt")
let PROBLEM_HASH_DIR = Path.Combine (DATA_DIR, "problem_hash")
let PROBLEM_DIR      = Path.Combine (DATA_DIR, "problems")
let SOLUTION_DIR     = Path.Combine (DATA_DIR, "solutions")

(* -------------------------------------------------------------------------- *)

module WebTools =
    
    let APIKEY =
        let path = System.IO.Path.Combine (System.IO.Directory.GetCurrentDirectory(), "apikey.txt")
        if System.IO.File.Exists path then
            let stm = System.IO.File.OpenText path
            let apikey = stm.ReadToEnd()
            stm.Close()
            apikey
        else
            failwith "Need apikey.txt file"
    
    let createWebRequest (url : string) =
        let req = System.Net.WebRequest.Create url :?> System.Net.HttpWebRequest
        req.Headers.Add ("Accept-Encoding", "gzip")
        req.Headers.Add ("X-API-Key", APIKEY)
        req.AutomaticDecompression <- System.Net.DecompressionMethods.GZip
        req
    
    let encoding = new System.Text.UTF8Encoding false
    
    let sendGetRequest (url : string) (f : System.IO.Stream -> unit) =
        let req = createWebRequest url
        let res = req.GetResponse() :?> System.Net.HttpWebResponse
        if res.StatusCode <> System.Net.HttpStatusCode.OK then
            printfn "Network Error: %A" res.StatusCode
            printfn "%s" res.StatusDescription
        else
            res.GetResponseStream() |> f
        res.Close()
        

(* -------------------------------------------------------------------------- *)
// REST API 'Hello, world!'

module HelloWorld =
    [<DataContract>]
    type internal Hello() =
        [<DataMember>]
        let mutable ok : bool = false
        [<DataMember>]
        let mutable greeting : string = ""
        member this.Ok = ok
        member this.Greeting = greeting
    
    let test() =
        let f (stm : System.IO.Stream) =
            let ser = new Json.DataContractJsonSerializer (typeof<Hello>)
            let hel = ser.ReadObject stm :?> Hello
            printfn "ok=%A greeting=%A" hel.Ok hel.Greeting
        WebTools.sendGetRequest "http://2016sv.icfpcontest.org/api/hello" f

(* -------------------------------------------------------------------------- *)
// REST API 'Contest Status Snapshot Query'

module ContestStatusSnapshotQuery =
    [<DataContract>]
    type internal Snapshot() =
        [<DataMember>]
        let mutable snapshot_time : int64 = 0L
        [<DataMember>]
        let mutable snapshot_hash : string = ""
        member this.SnapshotTime = snapshot_time
        member this.SnapshotHash = snapshot_hash
    
    [<DataContract>]
    type internal ContestStatus() =
        [<DataMember>]
        let mutable ok : bool = false
        [<DataMember>]
        let mutable snapshots : Snapshot array = [||]
        member this.Snapshots = snapshots
    
    let getSnapshot() =
        let f (stm : Stream) =
            printfn "Parse JSON..."
            let ser  = new Json.DataContractJsonSerializer (typeof<ContestStatus>)
            let stat = ser.ReadObject stm :?> ContestStatus
            let fstm = File.OpenWrite SNAPSHOT_FILE
            let wrt  = new StreamWriter (fstm, WebTools.encoding)
            let len  = Array.length stat.Snapshots
            let snp  = Array.get stat.Snapshots (len - 1)
            printfn "time=%A hash=%A" snp.SnapshotTime snp.SnapshotHash
            printfn "Write file..."
            wrt.Write snp.SnapshotHash
            wrt.Close()
        printfn "Get Snapshot..."
        WebTools.sendGetRequest "http://2016sv.icfpcontest.org/api/snapshot/list" f
        printfn "Done."
    

(* -------------------------------------------------------------------------- *)
// REST API 'Blob Lookup'

module BlobLookup =
    let API = "http://2016sv.icfpcontest.org/api/blob/"
    
    [<DataContract>]
    type internal ProblemRank() =
        [<DataMember>]
        let mutable resemblance : float = 0.0
        [<DataMember>]
        let mutable solution_size : int = 0
    
    [<DataContract>]
    type internal Problem() =
        [<DataMember>]
        let mutable ranking : ProblemRank array = [||]
        [<DataMember>]
        let mutable publish_time : int64 = 0L
        [<DataMember>]
        let mutable solution_size : int = 0
        [<DataMember>]
        let mutable problem_id : int = 0
        [<DataMember>]
        let mutable owner : int = 0
        [<DataMember>]
        let mutable problem_size : int = 0
        [<DataMember>]
        let mutable problem_spec_hash : string = ""
        member this.ProblemID = problem_id
        member this.ProblemSpecHash = problem_spec_hash
    
    [<DataContract>]
    type internal Rank() =
        [<DataMember>]
        let mutable username : string = ""
        [<DataMember>]
        let mutable score : float = 0.0
    
    [<DataContract>]
    type internal User() =
        [<DataMember>]
        let mutable username : string = ""
        [<DataMember>]
        let mutable display_name : string = ""
    
    [<DataContract>]
    type internal ContestStatusSnapshot() =
        [<DataMember>]
        let mutable problems : Problem array = [||]
        [<DataMember>]
        let mutable snapshot_time : int64 = 0L
        [<DataMember>]
        let mutable leaderboard : Rank array = [||]
        [<DataMember>]
        let mutable users : User array = [||]
        member this.Problems = problems
    
    let getProblems() =
        if File.Exists SNAPSHOT_FILE then
            let f (stm : Stream) =
                printfn "Parse JSON..."
                let ser  = new Json.DataContractJsonSerializer (typeof<ContestStatusSnapshot>)
                let stat = ser.ReadObject stm :?> ContestStatusSnapshot
                printfn "Write all problem hash..."
                for prb in stat.Problems do
                    let file = prb.ProblemID.ToString() + ".txt"
                    let path = Path.Combine (PROBLEM_HASH_DIR, file)
                    if not (File.Exists path) then
                        let fstm = File.OpenWrite path
                        let wtr  = new StreamWriter (fstm, WebTools.encoding)
                        wtr.Write prb.ProblemSpecHash
                        wtr.Close()
                        printf "."
                printfn ""
            printfn "Read Snapshot hash file..."
            let fstm = File.OpenText SNAPSHOT_FILE
            let hash = fstm.ReadToEnd()
            fstm.Close()
            printfn "Get Problems..."
            WebTools.sendGetRequest (API + hash) f
            printfn "Done."
        else
            printfn "Need snapshot hash file."
    
    let getProblem (pid : int) =
        let file  = pid.ToString() + ".txt"
        let hpath = Path.Combine (PROBLEM_HASH_DIR, file)
        let fpath = Path.Combine (PROBLEM_DIR, file)
        if File.Exists fpath then
            printfn "Problem No.%d has already existed." pid
        elif File.Exists hpath then
            let f (stm : Stream) =
                let red  = new StreamReader (stm, WebTools.encoding)
                let fstm = File.OpenWrite fpath
                let wrt  = new StreamWriter (fstm, WebTools.encoding)
                let prb  = red.ReadToEnd()
                wrt.Write prb
                wrt.Close()
                printfn "Done."
            let fstm = File.OpenText hpath
            let hash = fstm.ReadToEnd()
            printfn "Download Problem No.%d." pid
            WebTools.sendGetRequest (API + hash) f
            fstm.Close()
        else
            printfn "Problem No.%d hash is not found." pid

(* -------------------------------------------------------------------------- *)
// REST API 'Solution Submission'

module SolutionSubmission =
    [<DataContract>]
    type internal Submited() =
        [<DataMember>]
        let mutable ok : bool = false
        [<DataMember>]
        let mutable problem_id : int = 0
        [<DataMember>]
        let mutable resemblance : float = 0.0
        [<DataMember>]
        let mutable solution_spec_hash : string = ""
        [<DataMember>]
        let mutable solution_size : int = 0
        member this.Resemblance = resemblance
    
    let submit (pid : int) =
        let file = pid.ToString() + ".txt"
        let path = Path.Combine (SOLUTION_DIR, file)
        if File.Exists path then
            printfn "Read Solution No.%d..." pid
            let stm = File.OpenText path
            let ans = System.Uri.EscapeUriString <| stm.ReadToEnd()
            stm.Close()
            let req = WebTools.createWebRequest "http://2016sv.icfpcontest.org/api/solution/submit"
            req.Method <- "POST"
            req.ServicePoint.Expect100Continue <- false
            let mode = System.IO.Compression.CompressionMode.Compress
            let stm = req.GetRequestStream()
            let wtr = new StreamWriter (stm, new System.Text.ASCIIEncoding())
            wtr.Write ("problem_id={0}&solution_spec={1}", pid,  ans)
            printfn "Post solution..."
            wtr.Close()
            let res = req.GetResponse() :?> System.Net.HttpWebResponse
            if res.StatusCode <> System.Net.HttpStatusCode.OK then
                printfn "Network Error: %A" res.StatusCode
                printfn "%s" res.StatusDescription
            else
                let rstm = res.GetResponseStream() 
                printfn "Parse JSON..."
                let ser  = new Json.DataContractJsonSerializer (typeof<Submited>)
                let sbm = ser.ReadObject rstm :?> Submited
                printfn "Resemblance: %f" sbm.Resemblance
                printfn "Done."
            res.Close()
        else
            printfn "Solution No.%d is not found." pid
    
(* -------------------------------------------------------------------------- *)

let readProblem path =
    let stm = File.OpenText path
    
    let divi (s : string) : Div =
        match s.Split [|'/'|] with
        | [|xs; ys|] -> (int64 xs, int64 ys)
        | [|xs|] -> (int64 xs, 1L)
        | _ -> (0L, 1L)
    let tap (s : string) =
        let sep  = s.Split [|','|]
        (divi sep.[0], divi sep.[1])
    let pline (n) : Divs =
        [| for _ in 1 .. n -> tap <| stm.ReadLine() |]
    let bpos (s : string) =
        let sep = s.Split [|' '|]
        (tap sep.[0], tap sep.[1])
    
    let pn = int <| stm.ReadLine()
    let polys  =
        [| for _ in 1 .. pn -> pline << int <| stm.ReadLine() |]
    let ln = int <| stm.ReadLine()
    let lines =
        [| for _ in 1 .. ln -> bpos <| stm.ReadLine() |]
    
    stm.Close()
    (polys, lines)

let writeSolution file (vs : Divs, fs : int array array, ds : Divs) =
    let path = Path.Combine (SOLUTION_DIR, file)
    if File.Exists path then
        File.Delete path
    let stm  = File.OpenWrite path
    let wtr  = new StreamWriter (stm, WebTools.encoding)
    let sgn xc xp = if xp < 0L then (-xc, -xp) else (xc, xp)
    wtr.WriteLine ("{0}", Array.length vs)
    for ((xc, xp), (yc, yp)) in vs do
        let (xcc, xpp) = sgn xc xp
        let (ycc, ypp) = sgn yc yp
        wtr.WriteLine ("{0}/{1},{2}/{3}", xcc, xpp, ycc, ypp)
    wtr.WriteLine ("{0}", Array.length fs)
    for fa in fs do
        wtr.Write ("{0}", Array.length fa)
        for i in fa do
            wtr.Write (" {0}", i)
        wtr.WriteLine()
    for ((xc, xp), (yc, yp))  in ds do
        let (xcc, xpp) = sgn xc xp
        let (ycc, ypp) = sgn yc yp
        wtr.WriteLine ("{0}/{1},{2}/{3}", xcc, xpp, ycc, ypp)
    wtr.Close()

let callSolver (pid : int) =
    let file = pid.ToString() + ".txt"
    let path = Path.Combine (PROBLEM_DIR, file)
    if File.Exists path then
        printfn "Read Problem No.%d..." pid
        let (polys, lines) = readProblem path
        printfn "Solving..."
        let ans = Solver.solve polys lines
        printfn "Write Solution..."
        writeSolution file ans
        printfn "Done."

let fakeAnswer (pid : int) =
    printfn "Make fake..."
    let file = pid.ToString() + ".txt"
    printfn "Write Solution..."
    writeSolution file Solver.idleAnswer
    printfn "Done."

(* -------------------------------------------------------------------------- *)
// Overflow

let readProblemOF path =
    let stm = File.OpenText path
    
    let divi (s : string) : DivOF =
        match s.Split [|'/'|] with
        | [|xs; ys|] -> (BigInteger.Parse xs, BigInteger.Parse ys)
        | [|xs|] -> (BigInteger.Parse xs, BigInteger.One)
        | _ -> (BigInteger.Zero, BigInteger.One)
    let tap (s : string) =
        let sep  = s.Split [|','|]
        (divi sep.[0], divi sep.[1])
    let pline (n) : DivsOF =
        [| for _ in 1 .. n -> tap <| stm.ReadLine() |]
    let bpos (s : string) =
        let sep = s.Split [|' '|]
        (tap sep.[0], tap sep.[1])
    
    let pn = int <| stm.ReadLine()
    let polys  =
        [| for _ in 1 .. pn -> pline << int <| stm.ReadLine() |]
    let ln = int <| stm.ReadLine()
    let lines =
        [| for _ in 1 .. ln -> bpos <| stm.ReadLine() |]
    
    stm.Close()
    (polys, lines)

let writeSolutionOF file (vs : DivsOF, fs : int array array, ds : DivsOF) =
    let path = Path.Combine (SOLUTION_DIR, file)
    if File.Exists path then
        File.Delete path
    let stm  = File.OpenWrite path
    let wtr  = new StreamWriter (stm, WebTools.encoding)
    let sgn xc (xp : BigInteger) =
        if xp.Sign < 0 then
            (xc * BigInteger.MinusOne, xp * BigInteger.MinusOne)
        else
            (xc, xp)
    wtr.WriteLine ("{0}", Array.length vs)
    for ((xc, xp), (yc, yp)) in vs do
        let (xcc, xpp) = sgn xc xp
        let (ycc, ypp) = sgn yc yp
        wtr.WriteLine ("{0}/{1},{2}/{3}", xcc, xpp, ycc, ypp)
    wtr.WriteLine ("{0}", Array.length fs)
    for fa in fs do
        wtr.Write ("{0}", Array.length fa)
        for i in fa do
            wtr.Write (" {0}", i)
        wtr.WriteLine()
    for ((xc, xp), (yc, yp))  in ds do
        let (xcc, xpp) = sgn xc xp
        let (ycc, ypp) = sgn yc yp
        wtr.WriteLine ("{0}/{1},{2}/{3}", xcc, xpp, ycc, ypp)
    wtr.Close()

let callSolverOF (pid : int) =
    let file = pid.ToString() + ".txt"
    let path = Path.Combine (PROBLEM_DIR, file)
    if File.Exists path then
        printfn "Overflow Pattern."
        printfn "Read Problem No.%d..." pid
        let (polys, lines) = readProblemOF path
        printfn "Solving..."
        let ans = Solver.solveOF polys lines
        printfn "Write Solution..."
        writeSolutionOF file ans
        printfn "Done."

(* -------------------------------------------------------------------------- *)

let goSolving () =
    let allfiles = Directory.GetFiles PROBLEM_HASH_DIR
    printfn "Solve all?"
    let an = stdin.ReadLine()
    let sk = an <> "yes"
    printfn "... %s" (if sk then "skip mode" else "all mode")
    let count = Array.length allfiles
    printfn "Start Point?"
    let xn = stdin.ReadLine()
    let sta =
        try
            let x = int xn
            if x > 0 && x <= count then
                x
            else
                1
        with
            | _ -> 1
    printfn "... Start Point %d" sta
    printfn "Stop Point? (be careful! query 1000 by one hour)"
    let bn = stdin.ReadLine()
    let stp =
        try
            let x = int bn
            if x > 0 && x <= count then
                x
            else
                count
        with
            | _ -> count
    printfn "... Stop Point %d" stp    
    let exan i =
        let file = i.ToString() + ".txt"
        let path = Path.Combine (SOLUTION_DIR, file)
        File.Exists path
    let rec loopf i ovfl =
        printfn "i=%d (%d)" i stp
        let ki =
            if i - 1 < Array.length allfiles then
                int <| Path.GetFileNameWithoutExtension allfiles.[i - 1]
            else
                1
        if i > stp then
            ovfl
        elif sk && (exan ki) then
            loopf (i + 1) ovfl
        else
            BlobLookup.getProblem ki
            let s1 = stdin.ReadLine()
            if s1 = "bye" then
                ovfl
            elif s1 = "skip" then
                loopf (i + 1) ovfl
            else
                try
                    callSolver ki
                    let s2 = stdin.ReadLine()
                    if s2 = "bye" then
                        ovfl
                    else
                        SolutionSubmission.submit ki
                        let s3 = stdin.ReadLine()
                        if s3 = "bye" then
                            ovfl
                        else
                            loopf (i + 1) ovfl
                with
                    | :? System.OverflowException as ex ->
                        let ov2 = (i, ki) :: ovfl
                        printfn "%s" ex.Message
                        printfn "fake?"
                        let s4 = stdin.ReadLine()
                        if s4 = "bye" then
                            ov2
                        elif s4 = "fake" then
                            fakeAnswer ki
                            SolutionSubmission.submit ki
                            let s5 = stdin.ReadLine()
                            if s5 = "bye" then
                                ov2
                            else
                                loopf (i + 1) ov2
                        else
                            callSolverOF ki
                            let s6 = stdin.ReadLine()
                            if s6 = "bye" then
                                ovfl
                            else
                                SolutionSubmission.submit ki
                                let s7 = stdin.ReadLine()
                                if s7 = "bye" then
                                    ovfl
                                else
                                    loopf (i + 1) ov2
                    | ex ->
                        printfn "%s" ex.Message
                        ovfl
    let ovfl = loopf sta []
    printfn "Overflows: %A" ovfl

let autoSolving () =
    let allfiles = Directory.GetFiles PROBLEM_HASH_DIR
    let count = Array.length allfiles
    printfn "Solve all?"
    let an = stdin.ReadLine()
    let sk = an <> "yes"
    printfn "... %s" (if sk then "skip mode" else "all mode")
    printfn "Start Point?"
    let xn = stdin.ReadLine()
    let sta =
        try
            let x = int xn
            if x > 0 && x <= count then
                x
            else
                1
        with
            | _ -> 1
    printfn "... Start Point %d" sta
    printfn "Stop Point? (be careful! query 1000 by one hour)"
    let bn = stdin.ReadLine()
    let stp =
        try
            let x = int bn
            if x > 0 && x < count then
                x
            else
                count
        with
            | _ -> count
    printfn "... Stop Point %d" stp
    printfn "Use Fake?"
    let cn = stdin.ReadLine()
    let fk = cn <> "no"
    printfn "... %s" (if fk then "use fake" else "no fake")
    let exan i =
        let file = i.ToString() + ".txt"
        let path = Path.Combine (SOLUTION_DIR, file)
        File.Exists path
    let rec loopf i ovfl =
        printfn "i=%d (%d)" i stp
        let ki =
            if i - 1 < Array.length allfiles then
                int <| Path.GetFileNameWithoutExtension allfiles.[i - 1]
            else
                1
        if i > stp then
            ovfl
        elif sk && (exan ki) then
            loopf (i + 1) ovfl
        else
            System.Threading.Thread.Sleep 1000
            BlobLookup.getProblem ki
            try
                callSolver ki
                System.Threading.Thread.Sleep 1000
                SolutionSubmission.submit ki
                loopf (i + 1) ovfl
            with
                | :? System.OverflowException as ex ->
                    printfn "%s" ex.Message
                    if fk then
                        fakeAnswer ki
                        System.Threading.Thread.Sleep 1000
                        SolutionSubmission.submit ki
                    else
                        callSolverOF ki
                        System.Threading.Thread.Sleep 1000
                        SolutionSubmission.submit ki
                    loopf (i + 1) ((i, ki) :: ovfl)
                | ex ->
                    printfn "%s" ex.Message
                    ovfl
    let ovfl = loopf sta []
    printfn "Overflows: %A" ovfl


(* -------------------------------------------------------------------------- *)

let init() =
    let _ = Directory.CreateDirectory DATA_DIR
    let _ = Directory.CreateDirectory PROBLEM_HASH_DIR
    let _ = Directory.CreateDirectory PROBLEM_DIR
    let _ = Directory.CreateDirectory SOLUTION_DIR
    ()

let usage() =
    printfn "Usage:"

[<EntryPoint>]
let main args =
    init()
    if args = [||] then
        usage()
    else
        let pcall (f : int -> unit) =
            try
                let pid = int <| Array.get args 1
                f pid
            with
                | ex ->
                    printfn "%s" ex.Message
                    usage()
        match Array.get args 0 with
        | "hello" ->
            HelloWorld.test()
        | "snaphash" ->
            ContestStatusSnapshotQuery.getSnapshot()
        | "status" ->
            BlobLookup.getProblems()
        | "problem" ->
            pcall BlobLookup.getProblem
        | "solve" ->
            pcall callSolver
        | "submit" ->
            pcall SolutionSubmission.submit
        | "try" ->
            goSolving()
        | "auto" ->
            autoSolving()
        | "solveOF" ->
            pcall callSolverOF
        | "fake" ->
            pcall fakeAnswer
        | _ ->
            usage()
    0


;;
