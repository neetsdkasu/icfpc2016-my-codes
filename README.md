ICFPC 2016 My Code
------------------
Author  : Leonardone @ NEETSDKASU  
License : MIT  
Language: F# 2.0  


##Development Environment

###Computer
OS  : Windows 7 Starter SP1 (32-bit)  
CPU : Intel Atom N570 1.66 GHz
MEM : 1.0 GB  

###Tools
F#  : Microsoft F# 2.0 Complier build 4.0.40219.1  
Make: Borland MAKE Version 5.2  


